{
    "general": {
        "meta": {
            "tags": "Tagged \"{{ tags }}\"",
            "page": "Page {{ page }}"
        },
        "404": {
            "title": "You seem to be lost.",
            "subtext_html": "Don't worry we will get you back on the track. You can either go <a href=\"\/\">home<\/a> or search <a href=\"\/collections\/all\">here<\/a>."
        },
        "breadcrumbs": {
            "home": "Home",
            "home_link_title": "Back to the home page"
        },
        "newsletter_form": {
            "newsletter_email": "email@example.com",
            "newsletter_header": "Receive news and collection updates",
            "input_placeholder": "Email Address",
            "submit_button": "Sign up",
            "confirmation": "Thanks for subscribing"
        },
        "search": {
            "no_results": "No search results",
            "no_results_html": "Your search for \"{{ terms }}\" did not yield any results.",
            "results_for_html": "Your search for \"{{ terms }}\" revealed the following:",
            "title": "Search for products on our site",
            "placeholder": "Search our store",
            "submit": "Search"
        }
    },
    "blogs": {
        "article": {
            "author_on_date_html": "Posted by {{ author }} on {{ date }}",
            "older_post": "Older Post",
            "newer_post": "Newer Post",
            "tags": "Tags",
            "comment_meta_html": "{{ author }} on {{ date }}",
            "read_more": "Read more"
        },
        "comments": {
            "title": "Leave a comment",
            "name": "Name",
            "email": "Email",
            "message": "Message",
            "post": "Post comment",
            "moderated": "Please note, comments must be approved before they are published",
            "success_moderated": "Your comment was posted successfully. We will publish it in a little while, as our blog is moderated.",
            "success": "Your comment was posted successfully! Thank you!",
            "with_count": {
                "one": "{{ count }} comment",
                "other": "{{ count }} comments"
            }
        },
        "sidebar": {
            "recent_articles": "Recent Articles",
            "categories": "Categories"
        }
    },
    "cart": {
        "general": {
            "title": "Cart",
            "subtitle": "Shopping Cart",
            "summary": "Order Summary",
            "remove": "Remove item",
            "note": "Special instructions for seller",
            "subtotal": "Subtotal",
            "shipping_at_checkout": "Shipping & taxes calculated at checkout",
            "update": "Update Bag",
            "checkout": "Checkout",
            "empty": "Your cart is currently empty.",
            "continue_browsing_html": "Continue browsing <a href=\"\/collections\/all\">here<\/a>.",
            "close_cart": "Close Bag",
            "savings_html": "You're saving {{ price }}",
            "reduce_quantity": "Reduce item quantity by one",
            "increase_quantity": "Increase item quantity by one",
            "add_more": "Don’t you want to add...",
            "duties_info": "Duties fees and taxes may apply to your order once it reaches your country. We cannot predict as it not responsible for paying these additional charges or customs procedures. We recommend that you contact your local Customs office to receive additional information."
        },
        "label": {
            "product": "Product",
            "price": "Price",
            "quantity": "Quantity",
            "color": "Color",
            "size": "Size",
            "qty": "Qty",
            "total": "Total Price"

        }
    },
    "collections": {
        "general": {
            "all_of_collection": "All {{ collection }}",
            "no_matches": "Sorry, there are no products in this collection",
            "link_title": "Browse our {{ title }} collection",
            "grid_view": "Grid view",
            "list_view": "List view",
            "items_with_count": {
                "one": "{{ count }} item",
                "other": "{{ count }} items"
            },
            "filter_by": "Filter by"
        },
        "sidebar": {
            "tags": "Tags"
        },
        "sorting": {
            "title": "Sort by",
            "featured": "Featured",
            "best_selling": "Best Selling",
            "az": "Alphabetically, A-Z",
            "za": "Alphabetically, Z-A",
            "price_ascending": "Price, low to high",
            "price_descending": "Price, high to low",
            "date_descending": "Date, new to old",
            "date_ascending": "Date, old to new"
        },
        "onboarding": {
            "modal_title": "This page will show all of your store's products",
            "no_products_html": "There are no products yet, but once you begin adding them they will show up here regardless if they are in a collection.",
            "add_product": "Add Product"
        }
    },
    "contact": {
        "form": {
            "name": "Name",
            "email": "Email",
            "phone": "Phone Number",
            "message": "Message",
            "send": "Send",
            "post_success": "Thanks for contacting us. We'll get back to you as soon as possible."
        }
    },
    "customer": {
        "account": {
            "title": "My account",
            "details": "Account Details",
            "view_addresses": "View Addresses",
            "return": "Return to Account Details"
        },
        "activate_account": {
            "title": "Activate Account",
            "subtext": "Create your password to activate your account.",
            "password": "Password",
            "password_confirm": "Confirm Password",
            "submit": "Activate Account",
            "cancel": "Decline Invitation"
        },
        "addresses": {
            "title": "Addresses",
            "default": "Default",
            "add_new": "Add a New Address",
            "edit_address": "Edit address",
            "first_name": "First Name",
            "last_name": "Last Name",
            "company": "Company",
            "address1": "Address 1",
            "address2": "Address 2",
            "city": "City",
            "country": "Country",
            "province": "Province",
            "zip": "Postal\/Zip Code",
            "phone": "Phone",
            "set_default": "Set as default address",
            "add": "Add Address",
            "update": "Update Address",
            "cancel": "Cancel",
            "edit": "Edit",
            "delete": "Delete",
            "no_addresses": "You haven't placed any addresses yet."
        },
        "login": {
            "title": "Login",
            "email": "Email",
            "password": "Password",
            "forgot_password": "Forgot your password?",
            "sign_in": "Sign in",
            "cancel": "Return to Store",
            "guest_title": "Continue as a guest",
            "guest_continue": "Continue"
        },
        "orders": {
            "title": "Order History",
            "order_number": "Order number",
            "date": "Date",
            "payment_status": "Payment Status",
            "fulfillment_status": "Fulfillment Status",
            "payment_status_mobile": "Payment",
            "fulfillment_status_mobile": "Fulfillment",
            "total": "Total",
            "none": "You haven't placed any orders yet."
        },
        "order": {
            "title": "Order",
            "date": "Placed on {{ date }}",
            "cancelled": "Order Cancelled on {{ date }}",
            "cancelled_reason": "Reason: {{ reason }}",
            "billing_address": "Billing Address",
            "payment_status": "Payment Status",
            "shipping_address": "Shipping Address",
            "fulfillment_status": "Fulfillment Status",
            "discount": "Discount",
            "shipping": "Shipping",
            "tax": "Tax",
            "product": "Item",
            "sku": "SKU",
            "price": "Price",
            "quantity": "Quantity",
            "total": "Total",
            "fulfilled_at": "Fulfilled {{ date }}",
            "subtotal": "Subtotal"
        },
        "recover_password": {
            "title": "Reset password",
            "email": "Email",
            "submit": "Submit",
            "cancel": "Cancel",
            "subtext": "We will send you an email to reset your password.",
            "success": "We've sent you an email with a link to update your password."
        },
        "reset_password": {
            "title": "Reset password",
            "subtext": "Enter a new password for {{ email }}",
            "password": "Password",
            "password_confirm": "Confirm Password",
            "submit": "Reset Password"
        },
        "register": {
            "title": "Create account",
            "first_name": "First Name",
            "last_name": "Last Name",
            "email": "Email",
            "password": "Password",
            "submit": "Create",
            "cancel": "Return to Store"
        }
    },
    "homepage": {
        "sections": {
            "frontpage_title": "Frontpage Collection",
            "featured_title": "Featured Collections",
            "news_title": "Latest News"
        },
        "onboarding": {
            "modal_title": "Almost there...",
            "no_products_html": "You have no products in your home page collection. This placeholder will appear until you <a href=\"\/admin\/collections?tutorial=Frontpage\">add a product to this collection<\/a>.",
            "add_product": "Add a Product",
            "product_title": "Example Product Title",
            "no_collections_html": "You don't have any collections to show here. <a href=\"\/admin\/custom_collections\">Add some collections<\/a> to go along with the default home page.",
            "add_collection": "Add a Collection",
            "collection_title": "Example Collection Title"
        }
    },
    "layout": {
        "navigation": {
            "menu": "Menu"
        },
        "drawers": {
            "browse": "Browse",
            "close_menu": "Close menu"
        },
        "cart": {
            "title": "Bag",
            "items_count": {
                "one": "item",
                "other": "items"
            }
        },
        "customer": {
            "account": "Account",
            "logged_in_as_html": "Logged in as {{ first_name }}",
            "log_out": "Log out",
            "log_in": "Log in",
            "create_account": "Create account"
        },
        "footer": {
            "social_title": "Get Connected",
            "newsletter_title": "Newsletter",
            "accepted_payments": "Accepted Payments"
        }
    },
    "products": {
        "general": {
            "previous_product_html": "&larr; Previous Product",
            "next_product_html": "Next Product &rarr;",
            "from_text_html": "From {{ price }}",
            "sale_price": "Sale price",
            "regular_price": "Regular price",
            "related_products_title": "Related products"
        },
        "product": {
            "description": "Billy! Notes",
            "sold_out": "Sold Out",
            "all_in_cart": "All in cart",
            "on_sale": "On Sale",
            "on_sale_from_html": "<strong>On Sale<\/strong> from {{ price }}",
            "unavailable": "Unavailable",
            "compare_at": "Compare at",
            "front": "Tag one",
            "back": "Tag two",
            "quantity": "Quantity",
            "add_to_cart": "Add to Cart",
            "adding_to_cart": "Adding to Cart...",
            "added_to_cart": "Added to Cart",
            "add_to_cart_1": "Add to",
            "add_to_cart_2": "Cart",
            "added_to_cart_1": "Added",
            "adding_to_cart_1": "Adding"
        }
    },
    "gift_cards": {
        "issued": {
            "title": "Here's your {{ value }} gift card for {{ shop }}!",
            "subtext": "Here's your gift card!",
            "disabled": "Disabled",
            "expired": "Expired on {{ expiry }}",
            "active": "Expires on {{ expiry }}",
            "redeem": "Use this code at checkout to redeem your gift card",
            "shop_link": "Start shopping",
            "print": "Print",
            "add_to_apple_wallet": "Add to Apple Wallet"
        }
    },
    "date_formats": {
        "month_day_year": "%B %d, %Y"
    }
}
