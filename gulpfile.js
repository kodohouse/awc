'use strict';

var gulp         = require('gulp'),
    sass         = require('gulp-ruby-sass'),
    rename       = require('gulp-rename'),
    uglify       = require('gulp-uglify'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    clean        = require('gulp-clean'),
    include      = require('gulp-include');

/**
 * Default task - 'gulp' command
 */
gulp.task('default', ['serve']);

/**
 * Static Server + watching source files
 */
gulp.task('serve', ['sass', 'scripts'], function() {
    gulp.watch('./app/sass/**/*', ['sass']);
    gulp.watch('./app/js/*', ['scripts']);
});

/**
 * Compile with gulp-ruby-sass + source maps
 */
gulp.task('sass', () =>
    sass('./app/sass/main.scss', {
        sourcemap: true
    })
    .on('error', sass.logError)
    .pipe(sourcemaps.init())
    .pipe(autoprefixer({
        browsers: ['last 4 versions'],
        cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./woolen-theme/assets'))
);

/**
 * Add js scripts
 */
gulp.task('scripts', function() {
    gulp.src(['./app/js/main.js'])
        .pipe(sourcemaps.init())
            .pipe(include())
            .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: './app/js'
        }))
        .pipe(gulp.dest('./woolen-theme/assets'))
});

gulp.task('clean', function() {
    gulp.src(['./woolen-theme/assets/main.css', './woolen-theme/assets/main.css.map', './woolen-theme/assets/main.js', './woolen-theme/assets/main.js.map'])
        .pipe(clean({force : true}));
});
