$(function() {

    var Site = {
        init: function () {
            Site.customFunction();
            Site.historyFunctions();
            Site.initLoader();
            Site.navigateMain();
            Site.subscribeMailchimp();
            Site.navigateTeam();
            Site.navigateProcess();
            Site.navigateHistory();
            Site.playStory();
            Site.homeIndicator();
            Site.sectionSlider();
        },

        customFunction: function() {
            console.log('RIOT.agency');
        },

        historyFunctions: function () {
            $('a[href="#"]').on('click', function (e) {
                e.preventDefault();
            });
        },

        initLoader: function () {
            var timeout = 3000;
            var hash = location.hash.split('#')[1];
            var side;
            if (hash) {
                timeout = 1000;
                if (hash === 'collection') {
                    side = 'left';
                } else {
                    side = 'right';
                }

                $('.website')
                    .addClass('website--initialized')
                    .addClass('website--' + side);
                $('.slider')
                    .addClass('slider--start slider--active')
                    .addClass('slider--' + side)
                    .find('.slider__trigger').addClass('active');
                $('.section-hide-' + side)
                    .hide();
                $('.card')
                    .removeClass('card card--right card--left')
                    .find('.card__link')
                    .remove();
            }

            $('#page').imagesLoaded()
            .always( function( instance ) {
                console.log('all images loaded');
            })
            .done( function( instance ) {
                setTimeout(function () {
                    Site.initHomepage();
                }, timeout);
            })
            .fail( function() {
                console.log('all images loaded, at least one is broken');
            })
            .progress( function( instance, image ) {
                var result = image.isLoaded ? 'loaded' : 'broken';
                console.log( 'image is ' + result + ' for ' + image.img.src );
            });
        },

        initHomepage: function () {
            function releaseCard() {
                if ($('.header').hasClass('header--active')) {
                    $('.hamburger').trigger('click');
                }
                var side = $(this).data('side');
                setTimeout(function () {
                    $('.website')
                        .addClass('website--' + side)
                        .attr('data-side', side);
                    $('.section-hide-' + side).hide();
                    Site.fixMobileH();
                }, 401);
                setTimeout(function () {
                    $('.slider')
                        .addClass('slider--active')
                        .addClass('slider--' + side);
                    setTimeout(function () {
                        $('.slider__trigger').addClass('active');
                    })
                }, 2000);
            }

            function switchSides() {
                var side = $('.website').attr('data-side') == 'right' ? 'left' : 'right';
                if ($('.header').hasClass('header--active')) {
                    $('.hamburger').trigger('click');
                }
                $('.slider').addClass('slider--center');
                $('.website').addClass('website--switching');
                setTimeout(function () {
                    $('.slider').toggleClass('slider--left slider--right');
                    $('.website')
                        .toggleClass('website--left website--right')
                        .attr('data-side', side);
                    Site.fixMobileH();
                }, 1250);
                setTimeout(function () {
                    if ($('.website').hasClass('website--store')) {
                        $('.website').removeClass('website--store')
                    }
                    $('html, body').scrollTop(0);
                    $('.section-hide-left, .section-hide-right').toggle();
                    $('.slider').removeClass('slider--center');
                    $('.website').removeClass('website--switching');
                }, 1850);
            }

            $('.website').addClass('website--loaded');

            $('.slider').addClass('slider--start');
            $('.card__link, .header__switch_--trigger').on('click', releaseCard)
            $('.slider__trigger, .header__switch').on('click', switchSides)
        },

        homeIndicator: function () {
            $('.section-left').each(function () {
                if (!$(this).find('.section--hidden').length) {
                    $('.indicator--left').append('<li><span></span></li>');
                }
            });
            $('.section-right').each(function () {
                if (!$(this).find('.section--hidden').length) {
                    $('.indicator--right').append('<li><span></span></li>');
                }
            });

            $('.indicator li:first-of-type').addClass('active');

            $('.indicator').on('click', 'li', function () {
                var side = $(this).closest('.indicator').attr('data-side');
                var index = $(this).index();
                var sectionId = '#' + $('.section-' + side).eq(index).attr('id');
                if (index == 0) {
                    sectionId = 0;
                }
                TweenLite.to(window, 1, {scrollTo:{y: sectionId}, ease:Power3.easeOut});
            })

            var lastScrollTop = 0;
            $(document).on('scroll', function(){
                var winH = $(window).height();
                var pos = $(window).scrollTop();
                var index = Math.floor(pos/winH);
                var side = $('.website').attr('data-side');

                index = index === $('.shopify-section.section-' + side).length ? index-1 : index;

                if ($('.shopify-section.section-' + side).eq(index).find('.section--light').length) {
                    $('.indicator--' + side).addClass('indicator--dark');
                } else {
                    $('.indicator--' + side).removeClass('indicator--dark');
                }
                $('.indicator--' + side).find('.active').removeClass('active');
                $('.indicator--' + side).find('li').eq(index).addClass('active');

            });
        },

        fixMobileH: function () {
            if ($(window).width() < 992) {
                var sectionH = $(window).height();
                $('.100vh').css('max-height', sectionH);
                $('.50vh').css('max-height', sectionH/2);
            }
        },

        navigateMain: function () {
            function toggleNav() {
                $('.hamburger').toggleClass('is-active');
                $('.header').toggleClass('header--active');
                $('body').toggleClass('header-is-active');
            }

            $('.hamburger').on('click', toggleNav);

            $('.header__nav ul a:not(".header__switch")').on('click', function () {
                toggleNav();
            });
        },

        sectionSlider: function () {
            $('.section-container-slider').slick({
                slidesToScroll: 1,
                speed: 500,
                infinite: false,
                swipe: false,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"></button>'
            });
        },

        subscribeMailchimp: function () {
            var $form = $('.mailchimp-form');

            if ($form.length > 0) {
                $form.on('submit', function ( event ) {
                    $form = $(this);
                    if (event) event.preventDefault();
                    if (validateEmail($form.find('input[type=email]'))) {
                        register($form);
                    }
                });
            }

            $form.find('input').on('keyup', function () {
                $(this).removeClass('input-error');
            })

            function validateEmail($input) {
                var email = $input.val();
                var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,7}$/;
                if (filter.test(email)) {
                    return true;
                    $input.removeClass('input-error')
                } else {
                    $input.addClass('input-error')
                    return false;
                }
            }

            function register($form) {
                $form.find('.errors').hide();
                $('.newsletter').removeClass('newsletter--error')
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serialize(),
                    cache: false,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    error: function(err) {
                        $('.newsletter').addClass('newsletter--error')

                    },
                    success: function(data) {
                        if (data.result == "success") {
                            console.log('success');
                            $('.newsletter').addClass('newsletter--success');
                        } else {
                            $('.newsletter').addClass('newsletter--error')
                        }
                    }
                });
            }

            function showErrors(message) {
                if ($form.hasClass('subscribe')) {
                    $('.popups--newsletter, #popup-error').addClass('active');
                    $('#popup-error').find('.errors').html(message).show().find('*').hide();
                } else {
                    $form.find('.errors').html(message).show().find('*').hide();
                }
            }
        },

        navigateTeam: function () {
            $('.team__slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            fade: false,
                            arrows: true,
                            prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon icon-left"></i></button>',
                            nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon icon-right"></i></button>'
                        }
                    }
                ]
            });

            $('.team-toggle').on('click', function (e) {
                e.preventDefault();
                $('.team').addClass('team--active');
                if ($(window).width() < 992) {
                    showMobileTeam();
                } else {
                    showDesktopTeam();
                }
            });

            $('.team__members .member').on('click', function (e) {
                e.preventDefault();
                var index = $(this).index();
                $('.team__slider').slick('slickGoTo', index, true);
                if ($(window).width() < 992) {
                    toggleMobileSlider();
                } else {

                }
            });

            $('.team__popup-close').on('click', toggleMobileSlider);
            $('.team__close').on('click', hideDesktopTeam);

            $(window).on('resize', function () {
                if ($(window).width() >= 992) {
                    setHeight();
                }
            });

            function showMobileTeam() {
                TweenLite.to(window, 1, {scrollTo:{y: "#team-members"}, ease:Power3.easeOut});
            }

            function showDesktopTeam() {
                TweenLite.to(window, 0.2, {scrollTo:{y: "#team"}, ease:Power3.easeOut, onComplete: function () {
                    $('.team-container').addClass('team-container--active');
                }});
                $('.team').addClass('team--active').css('z-index', '1');
                var popup = $('.team__popup')[0];
            }

            function hideDesktopTeam() {
                $('.team-container').removeClass('team-container--active');
                $('.team').removeClass('team--active');
                setTimeout(function () {
                    $('.team').css('z-index', '-1');
                }, 700)
            }

            function setHeight() {
                var maxH = $('.team__slider .member img').height();
                $('.team-container').css('max-height', maxH);
            }

            function toggleMobileSlider() {
                $('.team__popup').toggleClass('team__popup--active');
            }
        },

        navigateHistory: function () {
            function initDateSlider() {
                var date = $(this).find('.episode__date span').text();
                $('<li>' + date + '</li>').appendTo('.history__timeline ul');
                var dateLetters = date.split('');
                dateLetters.forEach(function (element, index) {
                    $('<li>' + element + '</li>').appendTo('.history__date ul:eq(' + index + ')');
                });
            }

            function initTimeline() {
                var dates = $('.history__timeline ul').find('li');
                var start = $(dates[0]).text();
                var diff;
                dates.each(function (index) {
                    if (index > 0) {
                        diff = ($(this).text() - start) * 5;
                        if (($(this).text() - dates[index].textContent) < 40) {
                            diff = diff + index*170;
                        }
                        $(this).css('left', diff).attr('data-position', diff);
                    } else {
                        $(this).attr('data-position', '0');
                    }
                })
            }

            function changeDate(slide) {
                var fontSize = $('.history__date').css('font-size').replace('px', '');
                console.log(fontSize);
                var dateDigits = $('.history__date ul');
                var transform = fontSize * (slide);
                dateDigits.each(function () {
                    $(this).css('transform', 'translateY(' + -transform + 'px)');
                });
            }

            function moveTimeline(currentSlide, nextSlide) {
                var transform = $('.history__timeline').find('li').eq(nextSlide).data('position');
                $('.history__timeline li').css('transform', 'translateX(' + -transform + 'px)');

                $('.history__timeline').find('li.past').removeClass('past');
                for (var i = 1; i < nextSlide+1; i++) {
                    $('.history__timeline li').eq(i).addClass('past');
                }
            }

            function goToDate() {
                var slide = $(this).index();
                $('.history__slider').slick('slickGoTo', slide);
            }

            $('.history__slider').slick({
                slidesToScroll: 1,
                speed: 500,
                infinite: false,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"></button>'
            });

            $('.history__slider .episode:not(.slick-cloned)').each(initDateSlider);
            initTimeline();

            $('.history__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                changeDate(nextSlide);
                moveTimeline(currentSlide, nextSlide);
            });

            $('.history__timeline ul').on('click', 'li', goToDate);
        },

        navigateProcess: function () {
            $('.process__contents .begin').on('click', function (e) {
                e.preventDefault();
                $('.steps').addClass('steps--active');
                var step = $(this).data('step')-1;
                $('.steps__slider').slick('slickGoTo', step, true);
                TweenLite.to(window, 1, {scrollTo:{y: "#steps"}, ease:Power3.easeOut});
            })

            $('.steps__slider').slick({
                slidesToScroll: 1,
                fade: true,
                speed: 500,
                infinite: false,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"></button>',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            fade: false
                        }
                    }
                ]
            });
        },

        playStory: function () {
            function loadVideo(video) {

                function showVideo() {
                    this.parentNode.classList.add('section__video--active');
                }
                function hideVideo() {
                    this.pause();
                    this.parentNode.classList.remove('section__video--active');
                    $('.section__video-loader').removeClass('loading');
                }

                var src = video.dataset.source;
                var mp4 = video.getElementsByTagName('source')[0];
                mp4.src = src;

                video.load();
                video.play();
                video.addEventListener('loadeddata', showVideo, false);
                video.addEventListener('ended', hideVideo, false);
                video.addEventListener('pause', hideVideo, false);
                video.addEventListener('click', hideVideo, false);

            }

            $('.button--video').on('click', function (e) {
                e.preventDefault();
                $('.section__video-loader').addClass('loading');
                loadVideo($(this).closest('.section').find('.section__video video')[0]);
            })

        }


    }

    var Account = {

        init: function () {
            Account.formEvents();
            Account.editAccount();
        },

        formEvents: function () {
            //
        },

        editAccount: function () {

            $('.account__info h3').on('click', function () {
                $(this).next().slideToggle();
                $(this).toggleClass('active');
            });

            $('.address__buttons [href="#edit"]').on('click', function () {
                var addressId = $(this).closest('.address').attr('id');
                $('#AddAddress').hide();
                $('#EditAddress-' + addressId).toggle();
            });

            $('.cancelEditing').on('click', function (e) {
                $(this).closest('.form').hide();
            });

            $('[href="#AddAddress"]').on('click', function () {
                $(this).hide();
                $('#AddAddress').show();
            });

            $('#cancelNewAddress').on('click', function (e) {
                $('[href="#AddAddress"]').show();
                $('#AddAddress').hide();
            });

            $('.address__buttons [href="#delete"]').on('click', function () {
                var addressId = $(this).closest('.address').attr('id');
                if (confirm('Are you sure you wish to delete this address?')) {
                    Shopify.postLink(
                        '/account/addresses/' + addressId,
                        {'parameters': {'_method': 'delete'}}
                    );
                }
            });

            if ($('#AddAddress').find('.errors').length > 0) {
                $('.account__info--addresses').find('h3').addClass('active').next().show();
                $('#AddAddress').show();
                $('[href="#AddAddress"]').hide();
            }

            $('.input--country').each(toggleProvince);
            $('.input--country').on('change', toggleProvince);

            function toggleProvince() {
                var provinceCount = $(this).closest('form').find('.input--province').find('option').length;
                var $provinceLabel = $(this).closest('form').find('.input--province').siblings('label');
                if (provinceCount === 0) {
                    $provinceLabel.hide();
                } else {
                    $provinceLabel.show();
                }
            }
        }
    }

    var Product = {

        init: function () {
            Product.initSelectric();
            Product.selectVariants();
            Product.addToCart();
            Product.initSlider();
            Product.toggleDescription();
        },

        selectVariants: function () {
            function updateSelection() {
                var product = $(this).closest('.product');
                var variants = $(this).siblings().removeClass('active');
                var selectedValue = $(this).addClass('active').attr('data-value').trim();
                product
                    .find('option[value="' + selectedValue + '"]')
                    .prop('selected', true)
                    .closest('select')
                    .change();

                if ($(this).parent().hasClass('product__option--color')) {
                    var variantImage = product.find(".product__select option:contains('" + selectedValue + "') ").data('image-url');
                    var variantImageIndex = product.find(".featured:not(.slick-cloned) img[src*='" + variantImage.split('2048x2048')[0] + "']").parent().data('slick-index');
                    $('.product__featured').slick('slickGoTo', variantImageIndex);
                }
            }

            $('.product__option li').on('click', updateSelection);

            $('.product__option--color li').each(function () {
                var hex = $(this).data('hex');
                var r = hexToRgb(hex).split(',')[0];
                var g = hexToRgb(hex).split(',')[1];
                var b = hexToRgb(hex).split(',')[2];
                var hsl = rgbToHsl(r, g, b);
                var lightness = hsl[2];
                if (lightness < 0.5) {
                    $(this).addClass('dark');
                }

            })
        },

        toggleDescription: function () {
            $('.product__description table tr:nth-of-type(1), .product__description table tr:nth-of-type(2)').addClass('expanded');
            $('.product__description table tr:nth-child(odd)').on('click', function () {
                if ($(this).hasClass('expanded')) {
                    $('.expanded').removeClass('expanded')
                } else {
                    $('.expanded').removeClass('expanded');
                    $(this).addClass('expanded');
                    $(this).next().addClass('expanded');
                }
            });
        },

        addToCart: function () {
            var $button = $('.button--cart');

            $button.on('click', function () {
                setAdding();
                addToCart();
            });

            function setAdding() {
                $button.addClass('button--adding');
            }

            function setAdded() {
                $button.removeClass('button--adding').addClass('button--added');
                setTimeout(function () {
                    $button.removeClass('button--added').blur();
                }, 3000);
                Cart.updateCartGeneral();
            }

            function addToCart() {
                var q = $button.attr('data-cart-qty');
                var id = $button.attr('data-variant-id');
            }
        },

        initSlider: function () {

            $('.product__featured').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                speed: 1,
                asNavFor: '.product__thumbnails'
            })

            $('.product__zoom').slick({
                slidesToShow: 1,
                arrows: false,
                fade: true,
                speed: 100
            });

            $('.product__thumbnails').slick({
                arrows: true,
                prevArrow: '<button type="button" class="slick-arrow slick-prev"><i class="icon icon-arrow"></i></button>',
                nextArrow: '<button type="button" class="slick-arrow slick-next"><i class="icon icon-arrow"></i></button>',
                slidesToShow: 4,
                asNavFor: '.product__featured',
                focusOnSelect: true
            });

            $('.zoom').mousemove(function(e){
                var h = $(this).find('img').height();
                var vptHeight = $(document).height();
                var y = -((h - vptHeight)/vptHeight) * e.pageY;

                $('.zoom img').css('top', y + "px");
            });

            $('.product__featured .featured').on('click', function () {
                var index = $(this).data('slick-index');
                $('.product__zoom').slick('slickGoTo', index);
                $('.product__zoom').addClass('active');
            });

            $('.product__zoom .zoom').on('click', function functionName() {
                $('.product__zoom').removeClass('active');
            })

        },

        initSelectric: function () {
            // initialize jquery.selectric plugin
            $('.selectric').selectric({
                disableOnMobile: false,
                nativeOnMobile: false,
                arrowButtonMarkup: '<span class="selectric-arrow"></span>',
                onInit: function (event, element) {

                },
                onSelect: function (event, element, index) {
                    var qty = index+1;
                    $('.button--cart').attr('data-cart-qty', qty);
                }
            });

        }

    }

    var Cart = {

        init: function () {
            Cart.removeItem();
            Cart.updateQuantity();
        },

        removeItem: function () {

            $('.item__remove').on('click', function (e) {
                e.preventDefault();
                var item = $(this).closest('.item');
                var id = item.attr('id');
                CartJS.updateItemById(id, 0, {}, {
                    success: function (data) {
                        Cart.updateCartGeneral();
                    }
                });
                item.addClass('item--byebye');
                setTimeout(function () {
                    item.slideUp(333);
                    if ($('.item:not(.item--byebye)').length == 0) {
                        location.reload();
                    }
                }, 500);
            });
        },

        updateQuantity: function () {

            $('.selectric.quantity').on('change', updateItem);

            function updateItem() {
                var item = $(this).closest('.item');
                var id = item.attr('id');
                var quantity = $(this)[0].value;
                CartJS.updateItemById(id, quantity, {}, {success: function (data) {
                    var itemTotal = item.find('.item__total').data('price') * quantity;
                    item.find('.item__total span').html(Shopify.formatMoney(itemTotal));
                    Cart.updateCartGeneral();
                }});
            }

            $('.item__update').on('click', function (e) {
                e.preventDefault();
            });
        },

        updateCartGeneral: function () {

            jQuery.getJSON('/cart.js', function(data) {
                $('.header__nav-bag span').html(data.item_count);
                $('.cart__total .value').html(Shopify.formatMoney(data.total_price));
            });
        }

    }

    // here we go!
    Product.init();
    Cart.init();
    Site.init();
    Account.init();
});

$(window).on('beforeunload', function() {
    $(window).scrollTop(0);
});

// HELPER FUNCTIONS

function hexToRgb(hex) {
    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;

    return r + "," + g + "," + b;
}

/**
 * Converts an RGB color value to HSL. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and l in the set [0, 1].
 *
 * @param   {number}  r       The red color value
 * @param   {number}  g       The green color value
 * @param   {number}  b       The blue color value
 * @return  {Array}           The HSL representation
 */
function rgbToHsl(r, g, b){
    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max == min){
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [h, s, l];
}
